def printArray array, name
  puts "Массив " + name
  array.each{ |e| print e.to_s + " "}
  puts
end

aLength = 12
bLength = 12
a = (1..aLength).map { rand 10 }
b = (1..bLength).map { rand 10 }
printArray a, "A"
printArray b, "B"
c = []
c += b.find_all{ |e| e.even? }
c += b.find_all{ |e| e.odd? }
c += a.find_all{ |e| e.odd? }
c += a.find_all{ |e| e.even? }
printArray c, "C"
gets
