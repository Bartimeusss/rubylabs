class MatrixCalculator
  def self.getMatrixA length
    (1..length).to_a.map { |row| (1..length).to_a.map { |e| e == row ? 1 : rand(10) }  }
  end

  def self.getMatrixB length
    (1..length).to_a.map { |row| (1..length).to_a.map { |e| rand(10) }  }
  end

  def self.printMatrix matrix
    matrix.each{ |row|
      row.map{ |e| print e.to_s + " "}
      puts
    }
  end

  def self.multiplyByNumber matrix, num
    matrix.map { |row| row.map { |e| e * num } }
  end

  def self.addMatrix firstMatrix, secondMatrix
    firstMatrix.each_with_index.map { |row, i| row.each_with_index.map { |e, j| e + secondMatrix[i][j] } }
  end

  def self.transposeMatrix matrix
    matrix.each_with_index.map{ |row, i| row.each_with_index.map{ |col, j| matrix[j][i]}}
  end

  def self.multiplyByMatrix firstMatrix, secondMatrix
    raise "Nonconformant arguments" if firstMatrix[0].size != secondMatrix.size
    Array.new(firstMatrix.size) do |i|
      Array.new(secondMatrix[0].size) do |j|
        secondMatrix.size.times.inject(0) { |acc, k| acc + firstMatrix[i][k] * secondMatrix[k][j] }
      end
    end
  end

  def self.traceMatrix matrix
    matrix.each_with_index.map { |row, i| row[i] }.inject(0){ |result, elem| result + elem }
  end
end

class VectorCalculator
  def self.getRandomVector length
    (1..length).to_a.map{ |i| rand(10) }
  end

  def self.printVector vector
    vector.each { |e| print e.to_s + " " }
    puts
  end

  def self.getScalarProduct vectorX, vectorY
    vectorX.each_with_index.map { |e, i| e * vectorY[i] }.inject(0){ |totalProduct, product| totalProduct + product }
  end

  def self.getExternalProduct vectorX, vectorY
    vectorX.map { |x| vectorY.map { |y| x * y } }
  end

  def self.getNormal vector
    Math.sqrt(VectorCalculator.getScalarProduct vector, vector)
  end
end

class MatrixVectorCalculator
  def self.getProductColumnVectorWithMatrix columnVector, matrix
    matrix.map{ |row| VectorCalculator.getScalarProduct row, columnVector }
  end

  def self.getProductRowVectorWithMatrix rowVector, matrix
    transposedMatrix = MatrixCalculator.transposeMatrix matrix
    MatrixVectorCalculator.getProductColumnVectorWithMatrix rowVector, transposedMatrix
  end
end

a = MatrixCalculator.getMatrixA 8
b = MatrixCalculator.getMatrixB 8
puts "Матрица A"
MatrixCalculator.printMatrix a
puts "\nМатрица B"
MatrixCalculator.printMatrix b
puts "\nB * 5"
MatrixCalculator.printMatrix (MatrixCalculator.multiplyByNumber b, 5)
puts "\nA + B"
MatrixCalculator.printMatrix (MatrixCalculator.addMatrix a, b)
puts "\nТранспонирование A"
MatrixCalculator.printMatrix (MatrixCalculator.transposeMatrix a)
puts "\nA * B"
MatrixCalculator.printMatrix (MatrixCalculator.multiplyByMatrix a, b)
puts "\nСлед B"
puts (MatrixCalculator.traceMatrix b)
puts
puts
x = VectorCalculator.getRandomVector 8
y = VectorCalculator.getRandomVector 8
puts "\nВектор X"
VectorCalculator.printVector x
puts "\nВектор Y"
VectorCalculator.printVector y
puts "\nСкалярное произведение X * Y"
puts (VectorCalculator.getScalarProduct x, y)
puts "\nВнешнее произведение X * Y"
MatrixCalculator.printMatrix (VectorCalculator.getExternalProduct x, y)
puts "\nНорма вектора X"
puts (VectorCalculator.getNormal x)
puts "\nПроизведение вектор-столбца X на матрицу A"
VectorCalculator.printVector (MatrixVectorCalculator.getProductColumnVectorWithMatrix x, a)
puts "\nПроизведение вектор-строки Y на матрицу B"
puts (MatrixVectorCalculator.getProductRowVectorWithMatrix y, b)
gets
