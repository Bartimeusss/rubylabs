class Point
  attr_accessor :x,	:y

  def initialize x, y
    @x = x
    @y = y
  end
end

class PolygonAreaCalculator
	def calculate points
		points << points[0]
		area = 0
		points.each_index{|i| area += points[i].x * points[i + 1].y - points[i].y * points[i + 1].x if i < points.size - 1}
		return area / 2.0
	end
end

points = [
	Point.new(50,162),
	Point.new(62,123),
	Point.new(71,92),
	Point.new(82,73),
	Point.new(112,58),
	Point.new(159,40),
	Point.new(217,34),
	Point.new(264,31),
	Point.new(299,29),
	Point.new(354,29),
	Point.new(402,27),
	Point.new(435,32),
	Point.new(476,77),
	Point.new(489,97),
	Point.new(503,140),
	Point.new(506,181),
	Point.new(508,219),
	Point.new(497,243),
	Point.new(483,248),
	Point.new(419,256),
	Point.new(382,228),
	Point.new(370,199),
	Point.new(362,151),
	Point.new(342,125),
	Point.new(288,121),
	Point.new(257,160),
	Point.new(248,211),
	Point.new(239,247),
	Point.new(207,267),
	Point.new(181,273),
	Point.new(119,271),
	Point.new(101,250),
	Point.new(87,217),
	Point.new(105,188),
	Point.new(81,179),
	Point.new(62,176)
]

testPoints = [
	Point.new(1,1),
	Point.new(4,2),
	Point.new(5,3)
]

calculator = PolygonAreaCalculator.new
puts calculator.calculate points
#puts calculator.calculate testPoints
gets