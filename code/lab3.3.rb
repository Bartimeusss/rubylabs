class Lab_3_3_Tasks
	def fourthTask
		n = getN
		sum = 0
		denominator = 0;
		for i in 1..n
			denominator += Math.sin i
			sum += 1 / denominator
		end
		sum
	end

	def fifthDifficultTask
		n = getN
		sum = 0
		n.times do
			sum = Math.sqrt(2 + sum)
		end
		sum
	end

	def getN
		puts "Введите n"
		gets.chomp!.to_i
	end
end

lab_3_3_Tasks = Lab_3_3_Tasks.new
puts lab_3_3_Tasks.fourthTask
puts lab_3_3_Tasks.fifthDifficultTask
gets
