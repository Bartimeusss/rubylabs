class Point
  attr_accessor :x, :y

  def initialize x, y
    @x, @y = x, y
  end

  def print
    puts "x = " + @x.to_s + ", f = " + @y.to_s
  end
end

class FunctionsCalculator
  def calculateYFunction x, c, n
    firstMember = x ** (((n - c)**2 + 4 * n * c)) / ((n**2 - c**2) * n).to_f
    secondMember = (x / n.to_f + 12) / (6 - c * x).to_f
    firstMember + secondMember
  end

  def calculateZFunction x, c, n
    cos4x = Math.cos(4 * x)
    cos2x = Math.cos(2 * x)
    sin2x = Math.sin(2 * x)
    firstMember = (1 - cos4x) / (cos2x ** (-2) - 1).to_f
    secondMember = (1 + cos4x) / (sin2x ** (-2) - 1).to_f
    thirdMember = Math.tan(2 * Math::PI / 9 - x)**(c / n.to_f)
    firstMember + secondMember + thirdMember
  end

  def firstTask c, n
    getCalculateMethod = lambda{|x| method(:calculateYFunction) }
    taskLoop c, n, 1, n, n + c, getCalculateMethod
  end

  def secondTask c, n
    getCalculateMethod = lambda{|x| method(:calculateZFunction) }
    taskLoop c, n, Math::PI / n, Math::PI, 3 / 2.0 * n + c, getCalculateMethod
  end

  def thirdTask c, n
    getCalculateMethod = lambda{|x|
      if (2 <= x and x < n)
        method(:calculateYFunction)
      else (n <= x and x < 2 * n)
        method(:calculateZFunction)
      end
    }
    taskLoop c, n, 2, c, 2 * n, getCalculateMethod
  end

  def taskLoop c, n, startValue, endValue, count, getCalculateMethod
    oneStep = (endValue - startValue) / (count - 1).to_f
    result = []
    x = startValue
    while x < endValue
      calculateMethod = getCalculateMethod.call x
      f = calculateMethod.call x, c, n
      result << Point.new(x, f)
      x += oneStep
    end
    result
  end
end

puts "Введите значение c"
c = gets.chomp!.to_f
puts "Введите значение N"
n = gets.chomp!.to_f
functionCalculator = FunctionsCalculator.new

puts "\nFirst result :"
functionCalculator.firstTask(c, n).each{|point| point.print}

puts "\nSecond result :"
functionCalculator.secondTask(c, n).each{|point| point.print}

puts "\nThird result :"
functionCalculator.thirdTask(c, n).each{|point| point.print}
gets
