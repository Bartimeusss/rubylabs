def functionStep i, x
  t = 2 * i - 1
  Math.cos(t) * x / t**2
end

def getResult a, b, n
  step = (b - a) / n.to_f
  sum = 0
  for i in 0..n
    x = a + i * step
    sum += functionStep i, x
  end
  sum
end

a = 0.1
b = 1
n = -1
while ((n < 20) or (n > 58))
  puts "Введите количество элементов ряда n"
  n = gets.chomp!.to_i
  puts "n должно быть от 20 до 58!" if ((n < 20) or (n > 58))
end
puts getResult a, b, n
gets
