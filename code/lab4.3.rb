def printMatrix matrix
  matrix.each{ |row|
    row.map{ |e| print e.to_s + " "}
    puts
  }
end

def calculateGauss coefficients, freeMembers, n
  solutions = Array.new n
  k = 0
  while k < n
    index = k
    # Перестановка строк
    for j in 0..(n - 1)
      temp = coefficients[k][j]
      coefficients[k][j] = coefficients[index][j]
      coefficients[index][j] = temp
    end
    temp = freeMembers[k]
    freeMembers[k] = freeMembers[index]
    freeMembers[index] = temp
    # Нормализация уравнений
    for i in k..(n - 1)
      temp = coefficients[i][k]
      for j in 0..(n - 1)
        coefficients[i][j] = coefficients[i][j] / temp.to_f
      end
      freeMembers[i] = freeMembers[i] / temp.to_f
      next if i == k   # уравнение не вычитать само из себя
      for j in 0..(n - 1)
        coefficients[i][j] = coefficients[i][j] - coefficients[k][j]
      end
      freeMembers[i] = freeMembers[i] - freeMembers[k]
    end
    k += 1
  end
  # обратная подстановка
  k = (n - 1)
  while k >= 0
    solutions[k] = freeMembers[k]
    for i in 0..(k - 1)
      freeMembers[i] = freeMembers[i] - coefficients[i][k] * solutions[k]
    end
    k -= 1
  end
  return solutions
end

n = -1
while ((n < 3) or (n > 9))
    puts "Введите n"
    n = gets.chomp!.to_i
    puts "n должно быть от 3 до 9!" if ((n < 3) or (n > 9))
end
computerNumber = 10
freeMembers = (1..n).to_a
coefficients = freeMembers.map { |row| freeMembers.map { |e| e == row ? 1 : computerNumber + 2 }  }
puts "Матрица коефициентов (A):"
printMatrix coefficients
puts "Свободные члены уравнений (B):"
puts freeMembers
solutions = calculateGauss coefficients, freeMembers, n
puts "Решения (X):"
solutions.each{|e| puts e}
gets
