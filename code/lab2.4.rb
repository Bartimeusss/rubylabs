class DecimalCalculator
	def simpleCalculate decimalValue
		decimalValue.to_s 2
	end
	def taskCalculate decimalValue
		splitArray = decimalValue.to_s.split '.'
		binaryFirstArray = []
		firstPart = splitArray[0].to_i
		begin 
			binaryFirstArray << firstPart % 2
			firstPart /= 2
		end while firstPart != 0
		binaryValue = binaryFirstArray.reverse!.join ""
		if splitArray.length == 2
			secondPart = decimalValue - decimalValue.to_i
			binarySecondArray = []
			while secondPart - secondPart.to_i != 0
				binarySecondItem = 2 * secondPart
				binarySecondArray << binarySecondItem.to_i
				secondPart = binarySecondItem - binarySecondItem.to_i
			end
			binaryValue += "." + (binarySecondArray.join "")
		end
		binaryValue
	end
end

decimalCalculator = DecimalCalculator.new
testDecimalValue = 0.6875
taskDecimalValue = 333

puts decimalCalculator.taskCalculate testDecimalValue
puts
puts decimalCalculator.simpleCalculate taskDecimalValue
puts decimalCalculator.taskCalculate taskDecimalValue

