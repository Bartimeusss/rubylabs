class BinaryCalculator
	def simpleCalculate binaryValue
		binaryValue.to_s.to_i 2
	end
	def taskCalculate binaryValue
		splitArray = binaryValue.to_s.split '.'
		normalValue = 0
		firstPart = splitArray[0].reverse.split("").map{|e| e.to_f}
		firstPart.each_index{|i| normalValue += firstPart[i] * 2**i}
		if splitArray.length == 2
			secondPart = splitArray[1].split("").map{|e| e.to_f}
			secondPart.each_index{|i| normalValue += secondPart[i] * 2**(-i-1)}
		end
		normalValue
	end
end

binaryCalculator = BinaryCalculator.new
testBinaryValue = 1110.1
taskBinaryValue = 10101010101010

puts binaryCalculator.simpleCalculate testBinaryValue
puts binaryCalculator.taskCalculate testBinaryValue
puts
puts binaryCalculator.simpleCalculate taskBinaryValue
puts binaryCalculator.taskCalculate taskBinaryValue

