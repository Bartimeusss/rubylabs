class DiapasonCalculator
	def calculate p, t, r
		p = p.to_f
		t = t.to_f
		r = r.to_f
		(p**r * (1 - p**(-t)))
	end
end

diapasonCalculator = DiapasonCalculator.new
puts diapasonCalculator.calculate 6, 15, 10
puts diapasonCalculator.calculate 3, 8, 0
gets