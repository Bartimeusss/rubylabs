class Student
  attr_accessor :firstName,
                :lastName,
                :middleName,
                :sex,
                :age,
                :courseNumber

  def initialize firstName, lastName, middleName, sex, age, courseNumber
    @firstName, @lastName, @middleName, @sex, @age, @courseNumber = firstName, lastName, middleName, sex, age, courseNumber
  end

  def printInfo
    puts "\n=========Student============"
    puts "Имя: " + @firstName
    puts "Фамилия: " + @lastName
    puts "Отчество: " + @middleName
    puts "Пол: " + @sex
    puts "Возраст: " + @age.to_s
    puts "Номер курса: " + @courseNumber.to_s
    puts "============================"
  end
end

class KeyValuePair
  attr_accessor :key, :value

  def initialize key, value
    @key, @value = key, value
  end
end

def addStudent students
  puts "\nВведите имя"
  firstName = gets.chomp!
  puts "Введите фамилию"
  lastName = gets.chomp!
  puts "Введите отчество"
  middleName = gets.chomp!
  puts "Введите пол"
  sex = gets.chomp!
  puts "Введите возраст"
  age = gets.chomp!.to_i
  puts "Введите номер курса"
  courseNumber = gets.chomp!.to_i

  student = Student.new firstName, lastName, middleName, sex, age, courseNumber
  students << student
end

def printStudents students
  if students.size == 0
    puts "Пока нет студентов"
    return
  end
  students.each { |student| student.printInfo }
end

def printMostMenCourse students
  begin
    puts students
    gets
    courses = students.map { |student| student.courseNumber }.uniq
    puts courses
    gets
    menInCourses =  courses.map { |course| KeyValuePair.new course, students.select { |student| student.courseNumber == course and student.sex == "male" } }
    puts menInCourses
    gets
    mostMenCourse = menInCourses.max_by { |menInCourse| menInCourse.value.size }.key
    puts mostMenCourse
    gets
    puts "\nБольше всего мужчин на " + mostMenCourse.to_s + " курсе"
  rescue Exception => msg
      puts msg
  end
end

def printMostPopularNames students
  begin
    menNames = students.select { |student| student.sex == "male" }.map { |student| student.firstName }
    menNamesByCount = menNames.uniq.map { |name| KeyValuePair.new name, menNames.select {|n| n == name}.size }
    mostPopularMenName = menNamesByCount.max_by { |e| e.value }.key

    womenNames = students.select { |student| student.sex == "female" }.map { |student| student.firstName }
    womenNamesByCount = womenNames.uniq.map { |name| KeyValuePair.new name, womenNames.select {|n| n == name}.size }
    mostPopularWomenName = womenNamesByCount.max_by { |e| e.value }.key

    puts "\nСамое популярное мужское имя - " + mostPopularMenName
    puts "Самое популярное женское имя - " + mostPopularWomenName
  rescue Exception => msg
      puts msg
  end
end

def printLastNamesByAlphabet students
  begin
    lastNames = students.map { |student| student.lastName }.sort
    puts "\nФамилии всех студентов в алфавитном порядке"
    puts lastNames
  rescue Exception => msg
      puts msg
  end
end

def printInitialsOfMostPopularWomenByAge students
  begin
    women = students.select { |student|
      student.sex == "female"
    }
    womenAges = women.map { |women| women.age }.uniq
    womenByAges = womenAges.map { |age| KeyValuePair.new age, women.select { |women|  women.age == age } }
    mostPopularWomenByAge = womenByAges.max_by { |womenByAge|
       womenByAge.value.size
    }.value
    puts "\nИнициалы всех студенток, возраст которых самый распространенный"
    mostPopularWomenByAge.each { |women|
      puts women.lastName[0].to_s + ". " + women.firstName[0].to_s + ". " + women.middleName[0].to_s + "."
    }
  rescue Exception => msg
      puts msg
  end
end

students = Array.new
commandIndex = -1
while commandIndex != 0
  puts "\nДоступные комманды:"
  puts "Добавить студента - 1"
  puts "Вывести всех студентов - 2"
  puts "Вывести курс, на котором больше всего мужчин - 3"
  puts "Вывести самые популярные мужское и женское имена - 4"
  puts "Вывести фамилии всех студентов в алфавитном порядке - 5"
  puts "Вывести инициалы всех студенток, возраст которых самый распространенный - 6"
  puts "Выход - 0"
  commandIndex = gets.chomp!.to_i
  case commandIndex
    when 1
      addStudent students
    when 2
      printStudents students
    when 3
      printMostMenCourse students
    when 4
      printMostPopularNames students
    when 5
      printLastNamesByAlphabet students
    when 6
      printInitialsOfMostPopularWomenByAge students
    when 0
      break
    else
      puts "Недопустимый номер комманды!"
      next
  end
end
