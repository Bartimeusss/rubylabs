def prm a, b, f, n
  h = (b - a) / n.to_f
  result = 0
  for i in 0..(n - 1)
    x = a + i * h
    result += f.call x
  end
  result *= h
  return result
end

def trp a, b, f, n
  h = (b - a) / n.to_f
  result = (f.call(a) + f.call(b)) / 2.0
  for i in 1..(n - 1)
    x = a + i * h
    result += f.call x
  end
  result *= h
end

def firstFunction x
  2 / (1 - 4 * x).to_f
end

def secondFunction x
  Math.asin(Math.sqrt x) / Math.sqrt(x * (1 - x))
end


puts "Введите точность вычисления интеграла (количество отрезков)"
n = gets.chomp!.to_i

firstIntegralByPrm = prm 2.2, -1.2, method(:firstFunction), n
firstIntegralByTrp = trp 2.2, -1.2, method(:firstFunction), n

secondIntegralByPrm = prm 0.2, 0.3, method(:secondFunction), n
secondIntegralByTrp = trp 0.2, 0.3, method(:secondFunction), n

puts "Первый интеграл по методу прямоугольников:"
puts firstIntegralByPrm
puts "Первый интеграл по методу трапеций:"
puts firstIntegralByTrp

puts "Второй интеграл по методу прямоугольников:"
puts secondIntegralByPrm
puts "Второй интеграл по методу трапеций:"
puts secondIntegralByTrp

gets
