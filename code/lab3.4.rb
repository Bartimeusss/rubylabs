class Lab_3_4_Calculator
	def factorial n
		n > 1 ? n * factorial(n - 1) : 1
	end

	def getFirstTaskItem n
		(factorial(n - 1).to_f / factorial(n + 1).to_f)**(n*(n + 1))
	end

	def getSecondTaskItem x, n
		((2 * ((-1)**n) * (2**(2*n) - 1)).to_f / factorial(2 * n).to_f)  * (n**(6/7.0)).to_f * (x**(2*n - 1)).to_f
	end

	def csch x
		2 / ((Math.exp x) - (Math.exp(-x)) )
	end

	def getThirdTaskItem n
		(factorial(3 * n - 2) * factorial(2 * n - 1)) / (factorial(4*n) * 5**(2*n) * factorial(2*n)).to_f
	end

	def getFirstTaskResult e
		generalLoop 2, e, method(:getFirstTaskItem)
	end

	def getSecondTaskResult x, e
		sum = generalLoop 1, e, method(:getSecondTaskItem), x
		1/x.to_f + sum
	end

	def getThirdTaskResult e
		generalLoop 1, e, method(:getThirdTaskItem)
	end

	def generalLoop n, e, itemMethod, x = nil
		puts "e = " + e.to_s
		current = 0.0
		sum = 0.0
		loop do
			previous = current
			current = x == nil ? itemMethod.call(n) : itemMethod.call(x, n);
			sum += current
			puts "n = "+ n.to_s + ", current = " + current.to_s + ", previous = " + previous.to_s + ", sum = " + sum.to_s
			break if (current - previous).abs.to_f <= e
			n += 1
		end
		sum
	end
end

lab_3_4_Calculator = Lab_3_4_Calculator.new
puts lab_3_4_Calculator.getFirstTaskResult 0.00001
puts lab_3_4_Calculator.csch 5
puts lab_3_4_Calculator.getSecondTaskResult 5, 0.00001
puts lab_3_4_Calculator.getThirdTaskResult 0.00001
gets
