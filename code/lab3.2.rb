class Lab_3_2_Calculator
	def calculateWithCase
		x = getX
		return "Task error!" if x == -4
		return case x
			when -3..0 then getFirstValue x
			when 1..12 then getSecondValue x
			else getThirdValue x
		end
	end

	def calculateWithIf
		x = getX
		return "Task error!" if x == -4
		return getFirstValue x  if -4 < x and x <= 0
		return getSecondValue x if 0 < x and x <= 12
		return getThirdValue x
	end

	private

	def getX
		puts "Введите x"
		gets.to_f
	end

	def getFirstValue x
		((x - 2).abs / (x**2 * Math.cos(x)))**(1/7)
	end

	def getSecondValue x
		1 / (Math.tan(x + 1 / (Math.exp x)) / (Math.sin x)**2) ** (2/7)
	end

	def getThirdValue x
		1 / (1 + x / (1 + x / (1 + x)))
	end
end

calculator = Lab_3_2_Calculator.new
puts calculator.calculateWithIf
puts calculator.calculateWithCase
gets
